package github.timguy.wiederhall;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.IntentRequest;
import com.amazon.speech.speechlet.LaunchRequest;
import com.amazon.speech.speechlet.SessionEndedRequest;
import com.amazon.speech.speechlet.SessionStartedRequest;
import com.amazon.speech.speechlet.SpeechletResponse;
import com.amazon.speech.speechlet.SpeechletV2;

import github.timguy.wiederhall.helper.AlexaSpeechletResponse;

public class WiederHallSpeechlet implements SpeechletV2 {
	private static final Logger log = LoggerFactory
			.getLogger(WiederHallSpeechlet.class);
	private static final String SLOT_ONE = "slotOne";

	@Override
	public void onSessionStarted(
			SpeechletRequestEnvelope<SessionStartedRequest> requestEnvelope) {
		log.info("onSessionStarted requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

	@Override
	public SpeechletResponse onLaunch(
			SpeechletRequestEnvelope<LaunchRequest> requestEnvelope) {
		log.info("onLaunch requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
		return handleHelp();
	}

	@Override
	public SpeechletResponse onIntent(
			SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
		log.info("onIntent requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());

		Intent intent = requestEnvelope.getRequest().getIntent();
		switch (intent.getName()) {
			case "wiederHallIntent" :
				return handleWiederhall(requestEnvelope);
			case "AMAZON.HelpIntent" :
				return handleHelp();
			case "AMAZON.StopIntent" :
				return AlexaSpeechletResponse.tell()
						.withText("Gestoppt. Tschüß.")
						.build();
			case "AMAZON.CancelIntent" :
				return AlexaSpeechletResponse.tell()
						.withText(
								"Abgebrochen. Tschüß.")
						.build();
			default : {
				log.error("Unknown intent: " + intent.getName());
				return AlexaSpeechletResponse.tell()
						.withText("Ich habe deine Absicht nicht verstanden")
						.build();
			}
		}
	}

	private SpeechletResponse handleHelp() {
		// Create the plain text output.
		String speechOutput = "Ich mach nicht viel. Rede mit mir und ich wiederhole alles. Sage Stop um aufzuhöhren.";

		String repromptText = "Sag mir was oder sag Stopp!";

		return AlexaSpeechletResponse.ask()
				.withRepromptText(repromptText)
				.withText(speechOutput)
				.withSimpleCardContent(speechOutput).build();
	}



    private SpeechletResponse handleWiederhall(SpeechletRequestEnvelope<IntentRequest> requestEnvelope) {
        Intent intent = requestEnvelope.getRequest().getIntent();

        Slot slotOne = intent.getSlot(SLOT_ONE);
        if(slotOne == null)
        {
            String msg = SLOT_ONE + " war leider nicht gefüllt.";
            log.info(msg);
            return AlexaSpeechletResponse.ask().withText(msg).withRepromptText("Sprich!").build();
        }
        else{
            log.info("Inhalt von " + SLOT_ONE + ": " + slotOne.getValue());
            String returnText = "du sagtest " + slotOne.getValue();
            return AlexaSpeechletResponse.ask().withText(returnText).withRepromptText("Sag mir noch was.").build();
        }
       
    }


	@Override
	public void onSessionEnded(
			SpeechletRequestEnvelope<SessionEndedRequest> requestEnvelope) {
		log.info("onSessionEnded requestId={}, sessionId={}",
				requestEnvelope.getRequest().getRequestId(),
				requestEnvelope.getSession().getSessionId());
	}

}
